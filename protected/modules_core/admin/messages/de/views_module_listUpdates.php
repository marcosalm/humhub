<?php
return array (
  '<strong>Modules</strong> directory' => '<strong>Verzeichnis</strong> der Module',
  'Installed version:' => 'Installierte Version:',
  'Latest compatible Version:' => 'Letzte kompatible Version:',
  'More info' => 'Mehr Informationen',
  'Update' => 'Aktualisieren',
);
