<?php
return array (
  '<strong>Modules</strong> directory' => '<strong>Verzeichnis</strong> der Module',
  'Are you sure? *ALL* module data will be lost!' => 'Bist Du sicher? *ALLE* Modul Daten gehen verloren!',
  'Are you sure? *ALL* module related data and files will be lost!' => 'Bist Du sicher? *ALLE* Modul Abhängigen Daten und Dateien gehen verloren!',
  'Configure' => 'Konfigurieren',
  'Disable' => 'Deaktivieren',
  'Enable' => 'Aktivieren',
  'More info' => 'Mehr Informationen',
  'Uninstall' => 'Deinstallieren',
);
