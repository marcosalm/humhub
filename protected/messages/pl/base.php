<?php
return array (
  'Account settings' => 'Ustawienia konta',
  'Administration' => 'Administracja',
  'Back to dashboard' => 'Powrót do pulpitu',
  'Collapse' => 'Zwiń',
  'Content Addon source must be instance of HActiveRecordContent or HActiveRecordContentAddon!' => '',
  'Could not determine content container!' => '',
  'Could not find content of addon!' => '',
  'Could not find requested module!' => '',
  'Delete' => 'Usuń',
  'Email settings' => 'Ustawienia email',
  'Error' => 'Błąd',
  'Expand' => 'Rozwiń',
  'Insufficent permissions to create content!' => '',
  'Invalid request.' => '',
  'It looks like you may have taken the wrong turn.' => '',
  'Keyword:' => '',
  'Latest news' => '',
  'Logout' => 'Wyloguj',
  'Menu' => 'Menu',
  'Module is not on this content container enabled!' => '',
  'My profile' => 'Mój profil',
  'New profile image' => '',
  'Nothing found with your input.' => '',
  'Oooops...' => '',
  'Results' => '',
  'Save' => 'Zapisz',
  'Search' => 'Szukaj',
  'Search for users and spaces' => '',
  'Show more results' => 'Pokaż więcej wyników',
  'Sorry, nothing found!' => 'Przepraszam, nic nie znaleziono!',
  'Space not found!' => '',
  'User Approvals' => '',
  'User not found!' => '',
  'Welcome to <strong>HumHub</strong>' => 'Witamy na <strong>HumHub</strong>',
  'You cannot create public visible content!' => '',
  'Your daily summary' => 'Podsumowanie dzienne',
);
